import React from 'react'
import { Button ,Modal} from 'react-bootstrap'

export const ModalLayout = ({handleClose, handleSubmitDelete}) => {
  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Delete</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Are you sure?
        <form onSubmit={handleSubmitDelete}>
          <Button
            type="submit"
            className="btn btn-danger mt-4"
          >
            Delete
          </Button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
