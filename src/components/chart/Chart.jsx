import React, { useEffect, useState } from "react";
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import axios from "axios";
import Swal from "sweetalert2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const Chart = ({ token }) => {
  const [data, setChartData] = useState({
    labels: [],
    amounts: [],
  });
  const [data2,setChartData2] = useState({
    label2:[],
    amounts2:[]
  })

  useEffect(() => {
    const transactionChart = axios
      .get("http://127.0.0.1:8000/api/index/chart", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const chartData = res.data.data;
        const labels = chartData.map((item) => item.hour);
        const amounts = chartData.map((item) => item.links);
        setChartData({ labels, amounts });
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          timer: 1000,
          title: error.response.message,
        });
      });
  }, [token]);

  const chartData = {
    labels: data.labels,
    datasets: [
      {
        label: "Chart links in hours",
        data: data.amounts,
        backgroundColor: "#124076",
        borderColor: "rgba(75, 192, 192, 1)",
        borderWidth: 1,
      },
    ],
  };

  const options = {
    scales: {
      x: {
        stacked: true,
      },
      y: {
        beginAtZero: true,
      },
    },
  };

  return <Bar data={chartData} options={options} />;
};
