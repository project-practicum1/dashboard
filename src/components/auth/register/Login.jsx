import { Box, Button, FormControl, InputLabel, Link, MenuItem, Select, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { Auth } from '../../../api/Auth';
import axios from 'axios';
import Swal from 'sweetalert2';

export const Login = () => {
  const [formData, setFormData] = useState({
    email_or_username: "",
    password: "",
  });
  const { setToken } = Auth();  
  const [errors, setErrors] = useState({});
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  
  const validateForm = () => {
    const newErrors = {};
  
    if (!formData.email_or_username) newErrors.email_or_username = "Email or Username is required";
    if (!formData.password) newErrors.password = "Password is required";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validateForm()) {
      try {
        const response = await axios.post('http://127.0.0.1:8000/api/login', formData);
        setToken(response.data.token, response.data.user);
      } catch (err) {
        console.log(err.response)
        Swal.fire({
          icon: 'error',
          title: 'Login Failed!',
          text: err.response.data.message
        });
      }
    };
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      sx={{
        maxWidth: 400,
        my: "25vh",
        mx: "auto",
        p: 2,
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h4" component="h1" fontWeight="600" gutterBottom>
        Login
      </Typography>
      <TextField
        label="Email or Username"
        name="email_or_username"
        type="text"
        value={formData.email_or_username}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.email_or_username}
        helperText={errors.email_or_username}
      />
      <TextField
        label="Password"
        name="password"
        type="password"
        value={formData.password}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.password}
        helperText={errors.password}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        fullWidth
        sx={{ mt: 2 }}
      >
        Login
      </Button>
      <div style={{ display: "flex" , justifyContent: "space-between"}}>
        <Link
          href="/forget-password"
          underline="hover"
          sx={{ display: "block", mt: 2, textAlign: "center" }}
        >
          Forgot Password?
        </Link>
        <Link
          href="/register-form"
          underline="hover"
          sx={{ display: "block", mt: 2, textAlign: "center" }}
        >
          Not to register?
        </Link>
      </div>
      
    </Box>
  )
}
