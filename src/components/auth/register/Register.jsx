import React, { useState } from "react";
import {
  TextField,
  Button,
  MenuItem,
  FormControl,
  InputLabel,
  Select,
  Box,
  Typography,
} from "@mui/material";
import { Auth } from "../../../api/Auth";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export const Register = () => {
  const { http } = Auth();
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    gender: "",
    password: "",
    password_confirmation: "",
  });
  const navigation = useNavigate();
  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  
  const validateForm = () => {
    const newErrors = {};
    if (!formData.username) {newErrors.username = "Username is required"} else if(formData.username.length < 6) {newErrors.username = "Username is must be at least 6 characters"};
    if (!formData.email) {newErrors.email = "Email is required"}
    if (!formData.gender) newErrors.gender = "Gender is required";
    if (!formData.password) newErrors.password = "Password is required";
    if (formData.password !== formData.password_confirmation)
      newErrors.password_confirmation = "Passwords do not match";
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  }; 
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData)
    if (validateForm()) {
      http
        .post("/register", {
          gender: formData.gender,
          email: formData.email,
          password: formData.password,
          username: formData.username,
          password_confirmation: formData.password_confirmation,
        })
        .then((res) => {
          navigation("/login");
        })
        .catch((error) => {
          Swal.fire({
            icon: 'error',
            title: 'All Fields are required',
            text: error.response.data.message,
          });
        });
    }
  };
  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      sx={{
        maxWidth: 400,
        my: "15vh",
        mx: "auto",
        p: 2,
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h4" component="h1" fontWeight="600" gutterBottom>
        Register
      </Typography>
      <TextField
        label="Username"
        name="username"
        value={formData.username}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.username}
        helperText={errors.username}
      />
      <TextField
        label="Email"
        name="email"
        type="email"
        value={formData.email}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.email}
        helperText={errors.email}
      />
      <FormControl fullWidth margin="normal" error={!!errors.gender}>
        <InputLabel>Gender</InputLabel>
        <Select
          name="gender"
          value={formData.gender}
          onChange={handleChange}
          label="Gender"
        >
          <MenuItem value="">
            <em>Select</em>
          </MenuItem>
          <MenuItem value="male">Male</MenuItem>
          <MenuItem value="female">Female</MenuItem>
        </Select>
        {errors.gender && (
          <Typography color="error">{errors.gender}</Typography>
        )}
      </FormControl>
      <TextField
        label="Password"
        name="password"
        type="password"
        value={formData.password}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.password}
        helperText={errors.password}
      />
      <TextField
        label="Confirm Password"
        name="password_confirmation"
        type="password"
        value={formData.password_confirmation}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.password_confirmation}
        helperText={errors.password_confirmation}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        fullWidth
        sx={{ mt: 2 }}
      >
        Register
      </Button>
    </Box>
  );
};
