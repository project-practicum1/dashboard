import React, { useState } from "react";
import {
  TextField,
  Button,
  Box,
  Typography,
} from "@mui/material";
import { Auth } from "../../../api/Auth";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export const ForgetPassword = () => {
  const { http } = Auth();
  const [formData, setFormData] = useState({
    email: "",
  });
  const navigation = useNavigate();
  const [errors, setErrors] = useState({});
  const [successMessage, setSuccessMessage] = useState('');

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const validateForm = () => {
    const newErrors = {};
    if (!formData.email) {
      newErrors.email = "Email is required";
    }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  }; 

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      http
        .put("/password/forgot_password", {
          email: formData.email,
        })
        .then((res) => {
          setFormData({ email: '' }); // Clear form fields
          setSuccessMessage('Password reset instructions sent to your email.');
          setTimeout(() => {
            setSuccessMessage('');
            navigation("/forget-password/reset");
          }, 3000); // Redirect to login page after 3 seconds
        })
        .catch((error) => {
          Swal.fire({
            icon: 'error',
            title: 'Incorrect Email!',
            text: '',
          });
        });
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      sx={{
        maxWidth: 400,
        my: "35vh",
        mx: "auto",
        p: 2,
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h4" component="h1" fontWeight="600" gutterBottom>
        Forget Password
      </Typography>
      <TextField
        label="Email"
        name="email"
        type="email"
        value={formData.email}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.email}
        helperText={errors.email}
      />
      {successMessage && (
        <Typography variant="body1" sx={{ color: 'green', mt: 2 }}>
          {successMessage}
        </Typography>
      )}
      <Button
        type="submit"
        variant="contained"
        color="primary"
        fullWidth
        sx={{ mt: 2 }}
      >
        Reset
      </Button>
    </Box>
  );
};
