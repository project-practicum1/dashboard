import React, { useState } from "react";
import {
  TextField,
  Button,
  Box,
  Typography,
} from "@mui/material";
import { Auth } from "../../../api/Auth";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export const ResetPassword = () => {
  const { http } = Auth();
  const [formData, setFormData] = useState({
    email: "",
    Otp: "",
    password: "",
    password_confirmation: "",
  });
  const navigation = useNavigate();
  const [errors, setErrors] = useState({});
  const [successMessage, setSuccessMessage] = useState('');

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const validateForm = () => {
    const newErrors = {};

    if (!formData.email) {
        newErrors.email = "Old Email is required";
      }
      if (!formData.Otp) {
        newErrors.Otp = "OTP is required";
      }
      if (!formData.password) {
        newErrors.password = "Password is required";
      }
      if (!formData.password_confirmation) {
        newErrors.password_confirmation = "Password Confirmation is required";
      }
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  }; 

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      http
        .post("/password/reset_password", {
          email: formData.email,
          Otp: formData.Otp,
          password: formData.password,
          password_confirmation: formData.password_confirmation,

        })
        .then((res) => {
          setFormData({ email: '' ,Otp:'', password:'', password_confirmation:''}); // Clear form fields
          setSuccessMessage('Password reset successfully.');
          setTimeout(() => {
            setSuccessMessage('');
            navigation("/login");
          }, 3000); // Redirect to login page after 3 seconds
        })
        .catch((error) => {
          Swal.fire({
            icon: 'error',
            title: 'Incorrect!',
            text: 'Please check your information and try again.',
          });
        });
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      sx={{
        maxWidth: 400,
        my: "28vh",
        mx: "auto",
        p: 2,
        border: "1px solid #ccc",
        borderRadius: 2,
        boxShadow: 3,
      }}
    >
      <Typography variant="h4" component="h1" fontWeight="600" gutterBottom>
        Reset Password
      </Typography>
      <TextField
        label="Old Email"
        name="email"
        type="email"
        value={formData.email}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.email}
        helperText={errors.email}
      />
      <TextField
        label="OTP"
        name="Otp"
        type="text"
        value={formData.Otp}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.Otp}
        helperText={errors.otp}
      />
      <TextField
        label="New Password"
        name="password"
        type="password"
        value={formData.password}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.password}
        helperText={errors.password}
      />
      <TextField
        label="Confirm New Password"
        name="password_confirmation"
        type="password"
        value={formData.password_confirmation}
        onChange={handleChange}
        fullWidth
        margin="normal"
        error={!!errors.password_confirmation}
        helperText={errors.password_confirmation}
      />
      {successMessage && (
        <Typography variant="body1" sx={{ color: 'green', mt: 2 }}>
          {successMessage}
        </Typography>
      )}
      <Button
        type="submit"
        variant="contained"
        color="primary"
        fullWidth
        sx={{ mt: 2 }}
      >
        Submit
      </Button>
    </Box>
  );
};

