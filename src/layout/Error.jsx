import React from "react";
import SideNav from "../components/SideNav";
import { Box, Typography } from "@mui/material";

export const Error = () => {
  return (
    <>
      <Box sx={{ display: "flex" ,marginTop:"3rem" , marginLeft:"1rem"}}>
        <SideNav />
        <h1>Erro</h1>
      </Box>
    </>
  );
};
