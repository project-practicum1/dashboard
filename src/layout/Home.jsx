import React, { useEffect, useState } from "react";
import SideNav from "../components/SideNav";
import {
  Alert,
  Box,
  ButtonBase,
  Card,
  CardContent,
  Grid,
  Link,
  Stack,
  Typography,
} from "@mui/material";
import {
  AccessibilityNewOutlined,
  CategoryOutlined,
  CommentBank,
  HouseOutlined,
  LanguageOutlined,
  LinkOffOutlined,
  LocalAtmOutlined,
  PeopleAltOutlined,
  WebStoriesOutlined,
} from "@mui/icons-material";
import { Auth } from "../api/Auth";
import { useNavigate } from "react-router-dom";
import { Chart } from "../components/chart/Chart";
import Swal from "sweetalert2";
import axios from "axios";
import Aos from "aos";
import { Button } from "bootstrap";

export const Home = () => {
  const { token } = Auth();
  const navigate = useNavigate();
  const [data, showData] = useState({
    user_count: "",
    role_count: "",
    links: "",
    type: [],
    messages: [],
  });
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if (!token) {
      navigate("/login");
    }

    const fetchData = async () => {
      try {
        const res = await axios.get(
          "http://127.0.0.1:8000/api/index/report/auth",
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        showData(res.data.data);
      } catch (e) {
        Swal.fire({
          icon: "error",
          timer: 1000,
        });
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [token, navigate]);

  useEffect(() => {
    Aos.init();
  }, []);

  return (
    <>
      <Box sx={{ display: "flex", marginTop: "3.5rem", marginLeft: "1rem" }}>
        <SideNav />
        <Box component="main" sx={{ flexGrow: 1, p: 2 }}>
          <Grid container spacing={2}>
            <Grid
              data-aos="fade-up"
              data-aos-anchor-placement="bottom-bottom"
              item
              xs={8}
            >
              <Stack spacing={2} direction={"row"}>
                <Card
                  sx={{ width: 30 + "%", height: 180 }}
                  style={{ backgroundColor: "#344C64", color: "#fff" }}
                >
                  <CardContent>
                    <PeopleAltOutlined />
                    <Typography gutterBottom variant="h5" component="div">
                      {data.user_count} Users
                    </Typography>
                    <Typography variant="body2">Total Users</Typography>
                  </CardContent>
                </Card>
                <Card
                  sx={{ width: 30 + "%", height: 180 }}
                  style={{ backgroundColor: "#577B8D", color: "#fff" }}
                >
                  <CardContent>
                    <LanguageOutlined/>
                    <Typography gutterBottom variant="h5" component="div">
                      {data.links} Websites
                    </Typography>
                    <Typography variant="body2">Total Website</Typography>
                  </CardContent>
                </Card>
                <Card
                  sx={{ width: 30 + "%", height: 180 }}
                  style={{ backgroundColor: "#EE4E4E", color: "#fff" }}
                >
                  <CardContent>
                    <AccessibilityNewOutlined />
                    <Typography gutterBottom variant="h5" component="div">
                      {data.role_count} Roles
                    </Typography>
                    <Typography variant="body2">Total Roles</Typography>
                  </CardContent>
                </Card>
              </Stack>
            </Grid>
            <Grid
              data-aos="fade-up"
              data-aos-anchor-placement="top-bottom"
              item
              xs={4}
            >
              <Stack spacing={2}>
                <Card sx={{ maxWidth: 600, height: 120, padding: 1 }}>
                  <p className="fw-bold">
                    <CategoryOutlined /> Type Links
                  </p>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    {data.type.map((type, index) => (
                      <button
                        style={{
                          margin: "5px",
                          borderRadius: "5px",
                          border: "none",
                          backgroundColor: "#124076",
                          color: "white",
                          padding: "10px"
                        }}
                       key={index}>{type.name}</button>
                    ))}
                  </div>
                </Card>
              </Stack>
            </Grid>
          </Grid>
          <Box height={20} />
          <Grid container spacing={2}>
            <Grid
              data-aos="fade-up"
              data-aos-anchor-placement="bottom-bottom"
              item
              xs={8}
            >
              <Card sx={{ height: 60 + "vh" }}>
                <CardContent>
                  {/* Line Chart */}
                  <Chart token={token} />
                </CardContent>
              </Card>
            </Grid>
            <Grid
              data-aos="fade-up"
              data-aos-anchor-placement="bottom-bottom"
              item
              xs={4}
            >
              <Typography variant="p" sx={{ paddingLeft: 2, fontWeight: "bold" }}>
                <CommentBank/>  Message Users
              </Typography>
              <Card
                sx={{
                  height: 60 + "vh",
                  overflowY: "scroll",
                  "&::-webkit-scrollbar": {
                    display: "none",
                  },
                }}
              >
                {data.messages.map((chat, index) => (
                  <CardContent key={index}>
                    <span style={{ color: "#a1a3a6" }}>{chat.created_at}: <span className="fw-bold text-uppercase" >{chat.last_name}</span></span>
                    <Box
                      sx={{
                        border: "0px solid #124076",
                        width: "100%",
                        padding: "15px",
                        height: "auto",
                        backgroundColor: "#EBE7E7",
                        textOverflow: "ellipsis",
                        borderTopRightRadius: "1.5rem",
                        borderBottomLeftRadius: "1.5rem",
                        display: "-webkit-box",
                        WebkitBoxOrient: "vertical",
                        overflow: "hidden",
                      }}
                    >
                      <span>{chat.messages}</span>
                    </Box>
                  </CardContent>
                ))}
              </Card>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </>
  );
};
