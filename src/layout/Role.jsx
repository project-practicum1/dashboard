import React, { useEffect, useState } from "react";
import SideNav from "../components/SideNav";
import { Box, CircularProgress, Typography } from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Dropdown, DropdownButton, Modal } from "react-bootstrap";
import { Delete, Edit, VisibilitySharp } from "@mui/icons-material";
import axios from "axios";
import { Auth } from "../api/Auth";
import Swal from "sweetalert2";
import { DeleteRole } from "./role/DeleteRole";
import { EditRole } from "./role/EditRole";
import { PermissionRole } from "./role/PermissionRole";

export const Role = () => {
  const [roleData, setRoleData] = useState([]);
  const [show, setShow] = useState(false);
  const { token } = Auth();
  const handleClose = () => {
    setShow(false);
    setEditModalShow(false);
    setDeleteModalShow(false);
    setEditPermission(false);
  };
  const handleShow = () => setShow(true);
  const [search, setSearch] = useState("");
  const [name, setName] = useState("");
  const [selectedPermissions, setSelectedPermissions] = useState([]);
  const [error, setError] = useState({});
  const [selectedUserId, setSelectedUserId] = useState();
  const [showEdit, setEditModalShow] = useState(false);
  const [showDelete, setDeleteModalShow] = useState(false);
  const [showEditPermission, setEditPermission] = useState(false);
  const [loading, setLoading] = useState(true);

  const handleShowEdit = (userId) => {
    setSelectedUserId(userId);
    setEditModalShow(true);
  };
  const handleShowDelete = (userId) => {
    setSelectedUserId(userId);
    setDeleteModalShow(true);
  };

  const handleShowEditPermission = (userId) => {
    setSelectedUserId(userId);
    setEditPermission(true);
  };

  const permissionsList = [
    { id: 1, name: "view_user", label: "View User" },
    { id: 2, name: "create_user", label: "Create User" },
    { id: 3, name: "edit_user", label: "Edit User" },
    { id: 4, name: "delete_user", label: "Delete User" },
    { id: 5, name: "view_role", label: "View Role" },
    { id: 6, name: "create_role", label: "Create Role" },
    { id: 7, name: "edit_role", label: "Edit Role" },
    { id: 8, name: "delete_role", label: "Delete Role" },
    { id: 9, name: "view_items", label: "View Items" },
    { id: 10, name: "create_items", label: "Create Items" },
    { id: 11, name: "edit_items", label: "Edit Items" },
    { id: 12, name: "delete_items", label: "Delete Items" },
  ];

  // view
  useEffect(() => {
    const fetchRole = async () => {
      try {
        setLoading(true);
        const response = await axios.get(
          "http://127.0.0.1:8000/api/role/index",
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
            params: {
              search,
            },
          }
        );
        setRoleData(response.data.roles);
      } catch (e) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: e.response.data.message,
        });
      } finally {
        setLoading(false);
      }
    };
    fetchRole();
  }, [search, token]);
  // view

  // add

  const handleCheckboxChange = (e) => {
    const { id, checked } = e.target;
    const permissionId = parseInt(id);

    if (checked) {
      setSelectedPermissions((prevSelectedPermissions) => [
        ...prevSelectedPermissions,
        permissionId,
      ]);
    } else {
      setSelectedPermissions((prevSelectedPermissions) =>
        prevSelectedPermissions.filter((pid) => pid !== permissionId)
      );
    }
  };
  const validate = () => {
    const errors = {};

    if (!name) {
      errors.name = "Role name is required";
    }

    setError(errors);
    return Object.keys(errors).length === 0;
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      try {
        const response = await axios.post(
          "http://127.0.0.1:800/api/role/add",
          {
            name,
            permission: selectedPermissions,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
          }
        );
        console.log(response.data.message);
        Swal.fire({
          icon: "success",
          title: "Role created successfully",
          text: "",
        }).then(() => {
          handleClose();
          window.location.reload();
        });
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "User create failed",
          text: `${err.response.message}`,
        });
      }
    }
  };
  return (
    <>
      <Box sx={{ display: "flex", marginTop: "3rem", marginLeft: "1rem" }}>
        <SideNav />
        <div class="container-fluid ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
            <div class="row ">
              <div class="col-sm-3 mt-5 mb-4 text-gred d-flex justify-content-between">
                <div className="search w-75 mx-1">
                  <form class="form-inline">
                    <input
                      class="form-control mr-sm-2"
                      type="search"
                      placeholder="Search  by name"
                      aria-label="Search"
                      value={search}
                      onChange={(e) => setSearch(e.target.value)}
                    />
                  </form>
                </div>
              </div>

              <div
                class="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred"
                style={{ color: "#124076" }}
              >
                <h2>
                  <b>Table Role</b>
                </h2>
              </div>
              <div class="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                <Button variant="primary" onClick={handleShow}>
                  Create role
                </Button>
              </div>
            </div>
            <div class="row">
              <div class="table-responsive ">
                <table class="table align-middle  table-striped table-hover table-bordered">
                  <thead>
                    <tr className="text-uppercase">
                      <th>Id</th>
                      <th>Name</th>
                      <th>Permissions</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {loading ? (
                      <tr>
                        <td colSpan="9">
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <CircularProgress />
                          </Box>
                        </td>
                      </tr>
                    ) : (
                      roleData.map((role, index) => (
                        <tr key={index}>
                          <td>{role.id}</td>
                          <td>{role.name}</td>
                          <td>
                            {role.permissions.map(
                              (permission, permissionIndex) => (
                                <span
                                  key={permissionIndex}
                                  className="badge bg-success mx-1"
                                >
                                  {permission.name}
                                </span>
                              )
                            )}
                          </td>
                          <td>
                            <button
                              className="edit"
                              title="Edit"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "green",
                                objectFit: "cover",
                              }}
                              data-toggle="tooltip"
                              onClick={() => handleShowEditPermission(role.id)}
                            >
                              <VisibilitySharp />
                            </button>
                            <button
                              className="edit"
                              title="Edit"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "blue",
                                objectFit: "cover",
                              }}
                              data-toggle="tooltip"
                              onClick={() => handleShowEdit(role.id)}
                            >
                              <Edit />
                            </button>
                            <button
                              className="delete"
                              title="Delete"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "red",
                              }}
                              onClick={() => handleShowDelete(role.id)}
                            >
                              <Delete />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            {/* Model Box Finsihs */}
            <div className="model_box ">
              <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: "4rem" }}
              >
                <Modal.Header closeButton>
                  <Modal.Title>Add Role</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <input
                        type="text"
                        className={`form-control ${
                          error.name ? "is-invalid" : ""
                        }`}
                        id="name"
                        value={name}
                        aria-describedby="emailHelp"
                        placeholder="Enter Role Name"
                        onChange={(e) => {
                          setName(e.target.value);
                          setError((prevErrors) => ({
                            ...prevErrors,
                            name: "",
                          }));
                        }}
                        required
                      />
                      {error.name && (
                        <div className="invalid-feedback">{error.name}</div>
                      )}
                    </div>
                    <label className="mt-3">Permissions</label>
                    <div className="d-flex mt-2 justify-content-between">
                      <div className="w-50">
                        {permissionsList
                          .slice(0, Math.ceil(permissionsList.length / 2))
                          .map((permission) => (
                            <div className="form-check" key={permission.id}>
                              <input
                                className="form-check-input"
                                type="checkbox"
                                id={permission.id.toString()}
                                checked={selectedPermissions.includes(
                                  permission.id
                                )}
                                onChange={handleCheckboxChange}
                              />
                              <label
                                className="form-check-label"
                                htmlFor={permission.id.toString()}
                              >
                                {permission.label}
                              </label>
                            </div>
                          ))}
                      </div>
                      <div className="w-50">
                        {permissionsList
                          .slice(Math.ceil(permissionsList.length / 2))
                          .map((permission) => (
                            <div className="form-check" key={permission.id}>
                              <input
                                className="form-check-input"
                                type="checkbox"
                                id={permission.id.toString()}
                                checked={selectedPermissions.includes(
                                  permission.id
                                )}
                                onChange={handleCheckboxChange}
                              />
                              <label
                                className="form-check-label"
                                htmlFor={permission.id.toString()}
                              >
                                {permission.label}
                              </label>
                            </div>
                          ))}
                      </div>
                    </div>
                    <button type="submit" className="btn btn-success mt-4">
                      Add
                    </button>
                  </form>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Model add */}
            </div>
            {/* Model delete */}
            <div className="model_box ">
              {showDelete && (
                <DeleteRole
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>

            {/* Model edit name */}
            <div className="model_box">
              {showEdit && (
                <EditRole
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>

            {/* Model edit name */}
            <div className="model_box">
              {showEditPermission && (
                <PermissionRole
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>
          </div>
        </div>
      </Box>
    </>
  );
};
