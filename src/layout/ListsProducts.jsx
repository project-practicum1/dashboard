import {
  Box,
  Card,
  CardContent,
  CardMedia,
  CircularProgress,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import SideNav from "../components/SideNav";
import Aos from "aos";
import "aos/dist/aos.css";
import axios from "axios";
import { Auth } from "../api/Auth";

export const ListsProducts = () => {
  useEffect(() => {
    Aos.init();
  }, []);
  const { token } = Auth();
  const [dataItem, setDataItem] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const fetchProperties = async () => {
      try {
        const data = await axios.get("http://127.0.0.1:8000/api/record/links", {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json",
          },
        });
        setDataItem(data.data.message.data);
        console.log(data.data.message.data);
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: err.response.data.message,
        });
      } finally {
        setLoading(false);
      }
    };
    fetchProperties();
  }, [token]);

  return (
    <>
      <Box sx={{ display: "flex", marginTop: "5rem", marginLeft: "1rem" }}>
        <SideNav />
        <div style={{ display: "flex", flexWrap: "wrap", width: "100%" }}>
          {loading ? (
            <div style={{ width: "100%", height: "100%" }}>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <CircularProgress />
              </Box>
            </div>
          ) : (
            dataItem.map((item, index) => (
              <Card
                data-aos="zoom-in"
                sx={{ width: 345, margin: 1 }}
                key={index}
              >
                <CardMedia
                  component="img"
                  alt={item.name}
                  height="180"
                  image={
                    item.avatar ||
                    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                  }
                  sx={{
                    borderRadius: "50%",
                    width: "100px",
                    height: "100px",
                    margin: "auto",
                    marginTop: "16px",
                  }}
                />
                <CardContent sx={{ textAlign: "center" }}>
                  <Typography
                    gutterBottom
                    variant="h5"
                    component="div"
                    sx={{ color: "#344C64" }}
                  >
                    {item.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary" sx={
                    {
                      display: "-webkit-box",
                      WebkitLineClamp: 3,
                      WebkitBoxOrient: "vertical",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                    }
                  }>
                    {item.link_info[0].description_link}
                  </Typography>
                  <Typography
                      variant="body2"
                      sx={{
                        color: "#1976d2",
                        marginTop: "8px",
                        cursor: "pointer",
                      }}
                    >
                      <a
                        href={item.link_url && item.link_url[0] && item.link_url[0].url ? item.link_url[0].url : "#"}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        View Now
                      </a>
                    </Typography>
                    <Typography variant="body2" sx={{ marginTop: "8px", fontStyle: "italic" }}>
                      Created by: {item.user.username}
                    </Typography>
                </CardContent>
              </Card>
            ))
          )}
        </div>
      </Box>
    </>
  );
};
