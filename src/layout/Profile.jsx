import { Box, CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";
import SideNav from "../components/SideNav";
import { Button, Card, Form, Container, Row, Col } from "react-bootstrap";
import { FacebookOutlined, Telegram, X } from "@mui/icons-material";
import {  CardContent, Typography, Avatar, Grid } from "@mui/material";
import { Facebook, Pinterest, LinkedIn, Twitter, Instagram } from "@mui/icons-material";
import axios from "axios";
import { Auth } from "../api/Auth";
import Swal from "sweetalert2";

export const Profile = () => {
  const [data, setData] = useState({
    username: "",
    email: "",
    gender: "",
    address: "",
    phone: "",
    avatar: "",
    roles: [],
    info_user: {},
  });
  const [loading, setLoading] = useState(true);
  const { token } = Auth();
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const proData = await axios.get("http://127.0.0.1:8000/api/profile", {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json",
          },
        });
        setData(proData.data.user);
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: e.response.data.message,
        });
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [token]);
  return (
    <>
      <Box sx={{ height: "5vh" }} />
      <Box sx={{ display: "flex", marginTop: "4rem", marginLeft: "1rem" }}>
        <SideNav />
        {loading ? (
          <div style={{ width: "100%", height: "100%" }}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <CircularProgress />
            </Box>
          </div>
        ) : (
          <Container style={{ overflow: "hidden" }} fluid>
            <Row>
              <Col md="8">
                <Card>
                  <Card.Header>
                    <Card.Title as="h4">User Profile</Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <Form>
                      <Row className="pb-3">
                        <Col className="px-1" md="3">
                          <Form.Group>
                            <label>Username</label>
                            <Form.Control
                              value={data.username || ""}
                              type="text"
                              readOnly
                              disabled
                            ></Form.Control>
                          </Form.Group>
                        </Col>
                        <Col className="pl-1" md="4">
                          <Form.Group>
                            <label>Email address</label>
                            <Form.Control
                              type="email"
                              value={data.email || ""}
                              readOnly
                              disabled
                            ></Form.Control>
                          </Form.Group>
                        </Col>
                        <Col className="pl-1" md="4">
                          <Form.Group>
                            <label>Gender</label>
                            <Form.Select
                              className={`form-control`}
                              id="gender"
                              name="gender"
                              value={data.gender}
                              disabled
                            >
                              <option>Select</option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                            </Form.Select>
                          </Form.Group>
                        </Col>
                      </Row>
                      <Row className="pb-3">
                        <Col md="12">
                          <Form.Group>
                            <label>Address</label>
                            <Form.Control
                              placeholder="Home Address"
                              type="text"
                              value={data.address || "Null"}
                              readOnly
                              disabled
                            ></Form.Control>
                          </Form.Group>
                        </Col>
                      </Row>
                      <Row className="pb-3">
                        <Col className="pr-1" md="5">
                          <Form.Group>
                            <label>Phone</label>
                            <Form.Control
                              placeholder="Phone"
                              type="text"
                              value={data.phone || "Null"}
                              readOnly
                              disabled
                            ></Form.Control>
                          </Form.Group>
                        </Col>
                        <Col className="px-1" md="5">
                          <Form.Group>
                            <label>Role</label>
                            <Form.Control
                              placeholder="Role"
                              type="text"
                              value={data.roles
                                .map((role) => role.name)
                                .join(", ")}
                              readOnly
                              disabled
                            ></Form.Control>
                          </Form.Group>
                        </Col>
                      </Row>
                      <div className="clearfix"></div>
                    </Form>
                  </Card.Body>
                </Card>
              </Col>
              <Col md="4">
                <Card className="card-user">
                  <Card.Body>
                    <div className="author text-center">
                      <a href="#pablo">
                        <img
                          alt="..."
                          className="avatar border-gray"
                          style={{
                            width: "150px",
                            margin: "0 auto",
                            height: "100%",
                            objectFit: "cover",
                            borderRadius: "1rem",
                          }}
                          src={
                            data.avatar ||
                            "https://static.vecteezy.com/system/resources/previews/002/318/271/non_2x/user-profile-icon-free-vector.jpg"
                          }
                        ></img>
                      </a>
                      <h2 className="title mt-4 font-monospace text-capitalize fw-bold">
                        {data.username}
                      </h2>
                      <p
                        className="description text-info fw-semibold"
                        style={{ fontFamily: "cursive" }}
                      >
                        {data.info_user?.thumbnail}
                      </p>
                    </div>
                    <p className="description text-center w-50 mx-auto">
                      {data.info_user?.description || "Hi guys!"}
                    </p>
                    <div className="button-container d-flex justify-content-center">
                      <Button
                        className="btn-simple btn-icon"
                        href={data.info_user?.facebook_links || "#"}
                        variant="link"
                        style={{
                          backgroundColor: "#4267B2",
                          borderRadius: "0.5rem",
                        }}
                      >
                        <FacebookOutlined style={{ color: "#fff" }} />
                      </Button>
                      <Button
                        className="btn-simple btn-icon mx-2"
                        href={data.info_user?.twitter_links || "#"}
                        variant="link"
                        style={{
                          backgroundColor: "#14171A",
                          borderRadius: "0.5rem",
                        }}
                      >
                        <X style={{ color: "#fff" }} />
                      </Button>
                      <Button
                        className="btn-simple btn-icon"
                        variant="link"
                        href={data.info_user?.telegram_link || "#"}
                        style={{
                          backgroundColor: "#0088cc",
                          borderRadius: "0.5rem",
                        }}
                      >
                        <Telegram style={{ color: "#fff" }} />
                      </Button>
                    </div>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        )}
      </Box>
    </>
    
  );
};
