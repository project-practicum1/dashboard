import React from 'react'
import { ModalLayout } from '../../../components/Modal'
import Swal from 'sweetalert2';
import axios from 'axios';

export const DeleteType = ({userId, handleClose,token}) => {
  const handleSubmitDelete = async (e) => {
    e.preventDefault();
    try {
      await axios.delete(`http://127.0.0.1:8000/api/delete/link-category/${userId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-type": "application/json",
        },
      });
      Swal.fire({
        icon: "success",
        title: "Category Deleted Successfully",
        text: "",
        timer: 1000
      }).then(() => {
        handleClose();
        window.location.reload(); 
      });
    } catch (err) {
      Swal.fire({
        icon: "error",
        title: "Delete Unsuccessful",
        text: err.response.message.data,
        timer: 1000
      }).then(() => {
        handleClose();
      });
    }
  }

  return (
    <ModalLayout handleClose={handleClose} handleSubmitDelete={handleSubmitDelete}/>
  )
}
