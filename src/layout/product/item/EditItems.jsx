import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export const EditItems = ({ userId, token, handleClose }) => {
  const [formData, setFormData] = useState({
    name: "",
    link_categories_id: "",
    link_info: [
      {
        release_date: "",
        founder: "",
        description_link: "",
        detail: "",
        analysis: "",
      },
    ],
    avatar: "",
    status: "",
  });

  const [linkCate, setLinkCate] = useState("link_categories_id");
  const [cateId, setLinkCateId] = useState([]);

  const handleChange = (e) => {
    const { name, value, type, files } = e.target;
    if (type === "file") {
      setFormData({
        ...formData,
        avatar: files[0],
      });
    } else if (name.startsWith("link_info")) {
      const index = name.split(".")[1];
      const field = name.split(".")[2];
      const updatedLinkInfo = formData.link_info.map((info, i) =>
        i == index ? { ...info, [field]: value } : info
      );
      setFormData({
        ...formData,
        link_info: updatedLinkInfo,
      });
    } else if (name === "link_categories_id") {
      setFormData({
        ...formData,
        link_categories_id: value,
      });
    } else {
      setFormData({
        ...formData,
        [name]: value,
      });
    }
  };

  useEffect(() => {
    const fetchType = async () => {
      try {
        const response = await axios.get(
          `http://127.0.0.1:8000/api/index/link/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
          }
        );
        const data = response.data.message.data;
        setFormData({
          name: data.name,
          link_categories_id: data.links_category.id,
          status: data.status,
          link_info: data.link_info.map((info) => ({
            ...info,
          })),
          avatar: data.avatar ? new File([], data.avatar) : null, // Simulate File object if avatar exists
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    const fetchCateId = async () => {
      try {
        const [cateIdRes] = await Promise.all([
          axios.get("http://127.0.0.1:8000/api/index/link-category", {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
            params: {
              name: linkCate,
            },
          }),
        ]);
        setLinkCateId(cateIdRes.data.message.data);
        console.log(cateIdRes.data.message.data);
      } catch (error) {
        console.log(error.response.data.message);
      }
    };
    fetchCateId();
    fetchType();
  }, [userId, token, linkCate]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      await axios.patch(
        `http://127.0.0.1:8000/api/edit/links/${userId}`,

        {
          name: formData.name,
          link_categories_id: formData.link_categories_id,
          status: formData.status,
          link_info: formData.link_info.map((info) => ({
            release_date: info.release_date,
            founder: info.founder,
            description_link: info.description_link,
            detail: info.detail,
            analysis: info.analysis,
          })),
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (formData.avatar) {
        const avatarFormData = new FormData();
        avatarFormData.append("image", formData.avatar);

        await axios.post(
          `http://127.0.0.1:8000/api/image/link/edit/${userId}`,
          avatarFormData,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "multipart/form-data",
            },
          }
        );
      }

      Swal.fire({
        icon: "success",
        title: "Link updated",
        text: "",
        timer: 1000,
      });
      handleClose();
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      console.error("Error updating items:", error);
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "An error occurred while updating items.",
      });
    }
  };

  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Edit Link</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="form-group mx-2 my-2">
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={formData.name}
              onChange={handleChange}
              placeholder="Enter Name"
              required
            />
          </div>

          <div className="form-group mx-2 my-2">
            {/* <input
              type="number"
              className="form-control"
              id="link_categories_id"
              name="link_categories_id"
              value={formData.link_categories_id}
              onChange={handleChange}
              placeholder="Enter Link Category ID"
              required
            /> */}
            <select
              required
              className="form-control"
              name="link_categories_id"
              value={formData.link_categories_id}
              onChange={handleChange}
            >
              <option value="" disabled>
                Select Type
              </option>
              {cateId.map((cateId) => (
                <option key={cateId.id} value={cateId.id}>
                  {cateId.name}
                </option>
              ))}
            </select>
          </div>

          {formData.link_info.map((info, index) => (
            <div key={index}>
              <div className="form-group mx-2 my-2">
                <input
                  type="date"
                  className="form-control"
                  id={`release_date_${index}`}
                  name={`link_info.${index}.release_date`}
                  value={info.release_date}
                  onChange={handleChange}
                  placeholder="Enter Release Date"
                  required
                />
              </div>

              <div className="form-group mx-2 my-2">
                <input
                  type="text"
                  className="form-control"
                  id={`founder_${index}`}
                  name={`link_info.${index}.founder`}
                  value={info.founder}
                  onChange={handleChange}
                  placeholder="Enter Founder"
                  required
                />
              </div>

              <div className="form-group mx-2 my-2">
                <input
                  type="text"
                  className="form-control"
                  id={`description_link_${index}`}
                  name={`link_info.${index}.description_link`}
                  value={info.description_link}
                  onChange={handleChange}
                  placeholder="Enter Description Link"
                  required
                />
              </div>

              <div className="form-group mx-2 my-2">
                <textarea
                  className="form-control"
                  id={`detail_${index}`}
                  name={`link_info.${index}.detail`}
                  value={info.detail}
                  onChange={handleChange}
                  placeholder="Enter Detail"
                  required
                />
              </div>

              <div className="form-group mx-2 my-2">
                <textarea
                  className="form-control"
                  id={`analysis_${index}`}
                  name={`link_info.${index}.analysis`}
                  value={info.analysis}
                  onChange={handleChange}
                  placeholder="Enter Analysis"
                  required
                />
              </div>
            </div>
          ))}
          <div className="form-group mx-2 my-2">
            <select
              className="form-control"
              id="status"
              name="status"
              value={formData.status}
              onChange={handleChange}
              required
            >
              <option value="">Select Status</option>
              <option value="PUBLIC">Public</option>
              <option value="PRIVATE">Private</option>
            </select>
          </div>

          <div className="form-group mx-2 my-2">
            <input
              type="file"
              className="form-control"
              id="avatar"
              name="avatar"
              onChange={handleChange}
            />
          </div>

          <button type="submit" className="btn btn-success mt-4">
            Save
          </button>
        </form>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
