import React, { useEffect, useState } from "react";
import { Box, CircularProgress } from "@mui/material";
import SideNav from "../../components/SideNav";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from "react-bootstrap";
import { Delete, Edit, VisibilitySharp } from "@mui/icons-material";
import axios from "axios";
import { Auth } from "../../api/Auth";
import Swal from "sweetalert2";
import { DeleteOffer } from "./offer/DeleteOffer";
import { EditOffer } from "./offer/EditOffer";

export const Offer = () => {
  const [show, setShow] = useState(false);
  const [offer, setOffer] = useState([]);
  const [selectedUserId, setSelectedUserId] = useState();
  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [loading, setLoading] = useState(true);
  const [query, setQuery] = useState("");
  const { token } = Auth();
  const [roleBuyer, setRoleBuyer] = useState("buyer");
  const [formData, setFormData] = useState({
    links_id: "",
    url: "",
    status: "",
  });
  const [buyer, setBuyer] = useState([]);
  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    setShowEdit(false);
    setShowDelete(false);
  };
  const handleShowEdit = (user_id)=>{
    setSelectedUserId(user_id);
    setShowEdit(true);
  }
  const handleShowDelete = (user_id)=>{
    setSelectedUserId(user_id);
    setShowDelete(true);
  }


  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get(
          "http://127.0.0.1:8000/api/index/get-link",
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
            params: {
              search: query,
            },
          }
        );
        setOffer(res.data.message.data);
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: err.response.data.message,
        });
      } finally {
        setLoading(false);
      }
    };
    const fetchUsers = async () => {
      try {
        const [buyerRes] = await Promise.all([
          axios.get("http://127.0.0.1:8000/api/index/links", {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
            params: {
              name: roleBuyer,
            },
          }),
        ]);
        setBuyer(buyerRes.data.message.data);
      } catch (error) {
        console.log(error.response.data.message)
      }
    };
    fetchUsers();
    fetchData();
  }, [query, token, roleBuyer]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
    if (name === 'buyer') {
      setFormData({
        ...formData,
        links_id: value,
      });
    } 
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await axios.post(
      "http://127.0.0.1:8000/api/store/get-link",
      {
        links_id: formData.links_id,
        url: formData.url,
        status: formData.status,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-type": "multipart/form-data",
        },
      }
    );
    Swal.fire({
      icon: "success",
      title: "URL created!",
      text: "",
      timer: 1100,
    })
      .then(() => {
        handleClose();
        window.location.reload();
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "created fail",
          text: `${err.response.data.message}`,
        });
      });
  };

  return (
    <>
      <Box sx={{ display: "flex", marginTop: "3rem", marginLeft: "1rem" }}>
        <SideNav />
        <div class="container-fluid ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
            <div class="row ">
              <div class="col-sm-3 mt-5 mb-4 text-gred d-flex justify-content-between">
                <div className="search w-75 mx-1">
                  <form class="form-inline">
                    <input
                      class="form-control mr-sm-2"
                      type="search"
                      placeholder="Search by status"
                      aria-label="Search"
                      value={query}
                      onChange={(e) => setQuery(e.target.value)}
                    />
                  </form>
                </div>
              </div>

              <div
                class="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred"
                style={{ color: "#124076" }}
              >
                <h2>
                  <b>Table URL</b>
                </h2>
              </div>
              <div class="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                <Button variant="primary" onClick={handleShow}>
                  Create URL
                </Button>
              </div>
            </div>
            <div class="row">
              <div class="table-responsive ">
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr className="text-uppercase">
                      <th>Id</th>
                      <th>URL</th>
                      <th>Link Websites</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {loading ? (
                      <tr>
                        <td colSpan="9">
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <CircularProgress />
                          </Box>
                        </td>
                      </tr>
                    ) : (
                      offer.map((item, index) => (
                        <tr key={index}>
                          <td>{item.id}</td>
                          <td style={{
                            maxWidth: "200px",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap"
                          }} ><a href={item.url}>{item.url}</a></td>
                          <td>{item.links.name}</td>
                          <td>
                            <span
                              className={`badge ${
                                item.status
                                  ? item.status === "active"
                                    ? "bg-success"
                                    : item.status === "inactive"
                                    ? "bg-danger"
                                    : item.status === "reject"
                                    ? "bg-danger"
                                    : ""
                                  : ""
                              }`}
                            >
                              {item.status}
                            </span>
                          </td>
                          <td>
                            <button
                              class="edit"
                              title="Edit"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "blue",
                                objectFit: "cover",
                              }}
                              onClick={()=>handleShowEdit(item.id)}
                            >
                              <Edit />
                            </button>
                            <button
                              href="#"
                              class="delete"
                              title="Delete"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "red",
                              }}
                              onClick={()=>handleShowDelete(item.id)}
                            >
                              <Delete />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            {/* Model Box Finsihs */}
            <div className="model_box ">
              <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: "4rem" }}
              >
                <Modal.Header closeButton>
                  <Modal.Title>Add URL</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <select
                      required
                        className="form-control"
                        name="buyer"
                        value={formData.links_id}
                        onChange={handleChange}
                      >
                        <option value="" disabled>
                          Select Links
                        </option>
                        {buyer.map((buyers) => (
                          <option key={buyers.id} value={buyers.id}>
                            {buyers.name} 
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className="form-group mt-3">
                      <input
                      required
                        type="text"
                        className="form-control"
                        name="url"
                        placeholder="Enter URL"
                        value={formData.url}
                        onChange={handleChange}
                      />
                    </div>
                    <div className="form-group mt-3">
                      <select
                      required
                        className="form-control"
                        name="status"
                        value={formData.status}
                        onChange={handleChange}
                      >
                        <option value="" disabled>
                          Select status
                        </option>
                        <option value="active">Active</option>
                        <option value="inactive">Inactive</option>
                      </select>
                    </div>
                    <button type="submit" className="btn btn-success mt-4">
                      Add
                    </button>
                  </form>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Model Box Finsihs */}
            </div>

            {/* Model delete */}
            <div className="model_box">
              {showDelete && (
                <DeleteOffer userId={selectedUserId} token={token} handleClose={handleClose} />
              )}
            </div>
            {/* Model delete */}
            {/* Model delete */}
            <div className="model_box">
              {showEdit && (
                <EditOffer userId={selectedUserId} token={token} handleClose={handleClose} />
              )}
            </div>
            {/* Model delete */}

          </div>
        </div>
      </Box>
    </>
  );
};
