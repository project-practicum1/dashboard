import { Box, CircularProgress } from '@mui/material'
import React, { useEffect, useState } from 'react'
import SideNav from '../../components/SideNav'
import Swal from 'sweetalert2';
import axios from 'axios';
import { Auth } from '../../api/Auth';
import { Delete, Edit } from '@mui/icons-material';
import { Button, Modal } from 'react-bootstrap';
import {DeleteTrans} from './transaction/DeleteTrans';


export const Transaction = () => {
  const [show, setShow] = useState(false);
  const [viewTransaction,setViewTransaction] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedUserId, setSelectedUserId] = useState();
  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [query, setQuery] = useState("")
  const {token} = Auth();

  const handleClose = ()=>{
    setShow(false);
    setShowEdit(false);
    setShowDelete(false);
  };
  const handleShowDelete = (userId) => {
    setSelectedUserId(userId);
    setShowDelete(true);
  };

  useEffect(()=>{
    const fetchData = async ()=>{
      try {
        const res = await axios.get("http://127.0.0.1:8000/api/index/feedback",{
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json",
          },
          params:{
            search: query
          }
        })
        setViewTransaction(res.data.message.data);
      }catch(e){
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: err.response.data.message,
        });
      }finally{
        setLoading(false)
      }
    }
    fetchData();
  },[query,token]);


  return (
    <>
      <Box sx={{ display: "flex" ,marginTop:"3rem" , marginLeft:"1rem"}}>
        <SideNav />
        <div class="container-fluid ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
            <div class="row ">
              <div class="col-sm-3 mt-5 mb-4 text-gred d-flex justify-content-between">
                <div className="search w-75 mx-1">
                  <form class="form-inline">
                    <input
                      class="form-control mr-sm-2"
                      type="search"
                      placeholder="Search Username"
                      aria-label="Search"
                      value={query}
                      onChange={(e)=> setQuery(e.target.value)}
                    />
                  </form>
                </div>
              </div>

              <div
                class="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred"
                style={{ color: "#124076" }}
              >
                <h2>
                  <b>Table Comment</b>
                </h2>
              </div>

            </div>
            <div class="row">
              <div class="table-responsive ">
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr className="text-uppercase">
                      <th>Id</th>
                      <th>First name</th>
                      <th>Last name</th>
                      <th>email</th>
                      <th className='w-50'>message</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {loading ? (
                      <tr>
                        <td colSpan="9">
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <CircularProgress />
                          </Box>
                        </td>
                      </tr>
                    ) : (
                      viewTransaction.map((item, index) => (
                        <tr key={index}>
                          <td>{item.id}</td>
                          <td >{item.first_name}</td>
                          <td >{item.last_name}</td>
                          <td>{item.email}</td>
                          <td style={{
                            display: "-webkit-box",
                            WebkitBoxOrient:"vertical",
                            WebkitLineClamp: "3",
                            overflow: "hidden",
                            lineHeight: "1.5",
                          }}>{item.messages}</td>
                          <td>
                            <button
                              href="#"
                              class="delete"
                              title="Delete"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "red",
                              }}
                              onClick={()=>handleShowDelete(item.id)}
                            >
                              <Delete />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            {/* Model delete */}
            <div className="model_box">
              {showDelete && (
                <DeleteTrans userId={selectedUserId} token={token} handleClose={handleClose} />
              )}
            </div>
            {/* Model delete */}


          </div>
        </div>
      </Box>
    </>
  )
}
