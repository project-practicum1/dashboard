import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export const EditOffer = ({ userId, token, handleClose }) => {
  const [formData, setFormData] = useState({
    status: "",
    url: "",
  });
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get(
          `http://127.0.0.1:8000/api/index/get-link/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
          }
        );
        const data = res.data.message.data;
        setFormData(data);
        console.log(res.data.message.data);
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: err.response.data.message,
        });
      }
    };

    fetchData();
  }, [userId, token]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(formData);
    const res = await axios
      .put(
        `http://127.0.0.1:8000/api/edit/get-link/${userId}`,
        {
          status: formData.status,
          url: formData.url,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(() => {
        Swal.fire({
          icon: "success",
          title: "URL updated!",
          text: "",
          timer: 1100,
        });
        handleClose();
        setTimeout(() => {
          window.location.reload();
        });
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "URL failed to updated",
          text: err.response.message.data,
          timer: 1000,
        });
      });
  };

  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Edit Offer</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="form-group mt-3">
            <input
              type="text"
              className="form-control"
              name="url"
              placeholder="Enter URL"
              value={formData.url}
              onChange={handleChange}
            />
          </div>
          <div className="form-group mt-3">
            <select
              className="form-control"
              name="status"
              value={formData.status}
              onChange={handleChange}
            >
              <option value="" disabled>
                Select status
              </option>
              <option value="active">Active</option>
              <option value="inactive">Inactive</option>
            </select>
          </div>
          <button type="submit" className="btn btn-success mt-4">
            Edit
          </button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
