import React from "react";
import { ModalLayout } from "../../../components/Modal";
import axios from "axios";
import Swal from "sweetalert2";

export const DeleteOffer = ({ userId, token, handleClose }) => {
  const submit = async (e) => {
    e.preventDefault();
    try {
      await axios.delete(
        `http://127.0.0.1:8000/api/delete/get-link/${userId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json",
          },
        }
      );
      Swal.fire({
        icon: "success",
        title: "Offer Deleted Successfully",
        text: "",
        timer: 1000,
      }).then(() => {
        handleClose();
        window.location.reload();
      });
    } catch (err) {
      Swal.fire({
        icon: "error",
        title: "Delete Unsuccessful",
        text: err.response.message.data,
        timer: 1000,
      }).then(() => {
        handleClose();
      });
    }
  };
  return <ModalLayout handleClose={handleClose} handleSubmitDelete={submit} />;
};
