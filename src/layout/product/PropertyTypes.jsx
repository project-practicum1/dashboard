import React, { useEffect, useState } from "react";
import { Box, CircularProgress } from "@mui/material";
import SideNav from "../../components/SideNav";
import axios from "axios";
import { Auth } from "../../api/Auth";
import Swal from "sweetalert2";
import { Button, Modal } from "react-bootstrap";
import { Delete, Edit } from "@mui/icons-material";
import { DeleteType } from "./type/DeleteType";
import { EditType } from "./type/EditType";

export const PropertyTypes = () => {
  const [typePro, setTypePro] = useState([]);
  const [loading, setLoading] = useState(true);
  const { token } = Auth();
  const [show, setShow] = useState(false);
  const [name, setName] = useState("");
  const [selectedUserId, setSelectedUserId] = useState();
  const [showEdit, setEditModalShow] = useState(false);
  const [showDelete, setDeleteModalShow] = useState(false);
  const [error, setError] = useState({});
  const [query, setQuery] = useState("");

  const handleShow = () => setShow(true);
  const handleShowEdit = (userId) => {
    setSelectedUserId(userId);
    setEditModalShow(true);
  };
  const handleShowDelete = (userId) => {
    setSelectedUserId(userId);
    setDeleteModalShow(true);
  };
  const handleClose = () => {
    setShow(false);
    setEditModalShow(false);
    setDeleteModalShow(false);
  };

  useEffect(() => {
    const fetchType = async () => {
      try {
        setLoading(true);
        const data = await axios.get(
          "http://127.0.0.1:8000/api/index/link-category",
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
            params: {
              search: query,
            },
          }
        );
        setTypePro(data.data.message.data);
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: e.response.data.message,
        })
      } finally {
        setLoading(false);
      }
    };
    fetchType();
  }, [query,token]);
  const validate = () => {
    const errors = {};

    if (!name) {
      errors.name = "Role name is required";
    }

    setError(errors);
    return Object.keys(errors).length === 0;
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      try {
        const res = await axios.post(
          "http://127.0.0.1:8000/api/store/link-category",
          {
            name,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "multipart/form-data",
            },
          }
        );
        Swal.fire({
          icon: "success",
          title: "Created successfully",
          text: "",
          timer: 1200,
        }).then(() => {
          handleClose();
          window.location.reload();
        });
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "Failed!",
          text: err.response.data.message,
          timer: 1200,
        }).then(() => {
          handleClose();
        });
        console.log(err.response.data.message)
      }
    }
  };

  return (
    <>
      <Box sx={{ display: "flex", marginTop: "3rem", marginLeft: "1rem" }}>
        <SideNav />
        <div class="container-fluid ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
            <div class="row ">
              <div class="col-sm-3 mt-5 mb-4 text-gred d-flex justify-content-between">
                <div className="search w-75 mx-1">
                  <form class="form-inline">
                    <input
                      class="form-control mr-sm-2"
                      type="search"
                      placeholder="Search"
                      aria-label="Search"
                      value={query}
                      onChange={(e) => setQuery(e.target.value)}
                    />
                  </form>
                </div>
              </div>

              <div
                class="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred"
                style={{ color: "#124076" }}
              >
                <h2>
                  <b>Table Type</b>
                </h2>
              </div>
              <div class="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                <Button variant="primary" onClick={handleShow}>
                  Create Types
                </Button>
              </div>
            </div>
            <div class="row">
              <div class="table-responsive ">
                <table class="table align-middle  table-striped table-hover table-bordered">
                  <thead>
                    <tr className="text-uppercase">
                      <th>Id</th>
                      <th>Name</th>
                      <th>Created At</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {loading ? (
                      <tr>
                        <td colSpan="9">
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <CircularProgress />
                          </Box>
                        </td>
                      </tr>
                    ) : (
                      typePro.map((type, index) => (
                        <tr key={index}>
                          <td>{type.id}</td>
                          <td>{type.name}</td>
                          <td className="fw-bold">
                            {type.created_at.split("T")[0]}
                          </td>
                          <td>
                            <button
                              className="edit"
                              title="Edit"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "blue",
                                objectFit: "cover",
                              }}
                              data-toggle="tooltip"
                              onClick={() => handleShowEdit(type.id)}
                            >
                              <Edit />
                            </button>
                            <button
                              className="delete"
                              title="Delete"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "red",
                              }}
                              onClick={() => handleShowDelete(type.id)}
                            >
                              <Delete />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            {/* Model Box Finsihs */}
            <div className="model_box ">
              <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: "4rem" }}
              >
                <Modal.Header closeButton>
                  <Modal.Title>Add Category</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <input
                        type="text"
                        className={`form-control ${
                          error.name ? "is-invalid" : ""
                        }`}
                        id="name"
                        value={name}
                        aria-describedby="name for property types"
                        placeholder="Enter types of links"
                        onChange={(e) => {
                          setName(e.target.value);
                          setError((prevErrors) => ({
                            ...prevErrors,
                            name: "",
                          }));
                        }}
                        required
                      />
                      {error.name && (
                        <div className="invalid-feedback">{error.name}</div>
                      )}
                    </div>
                    <button type="submit" className="btn btn-success mt-4">
                      Add
                    </button>
                  </form>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Model add */}
            </div>

            {/* Model delete */}
            <div className="model_box ">
              {
                showDelete && (
                  <DeleteType userId={selectedUserId} token={token} handleClose={handleClose}/>
                )
              }
            </div>

            {/* Model edit name */}
            <div className="model_box ">
              {
                showEdit && (
                  <EditType userId={selectedUserId} token={token} handleClose={handleClose}/>
                )
              }
            </div>
            {/* Model edit name */}
          </div>
        </div>
      </Box>
    </>
  );
};
