import React, { useEffect, useState } from "react";
import { Box, CircularProgress } from "@mui/material";
import SideNav from "../../components/SideNav";
import { Button, Modal } from "react-bootstrap";
import { Delete, Edit } from "@mui/icons-material";
import { Auth } from "../../api/Auth";
import axios from "axios";
import Swal from "sweetalert2";
import { DeleteItems } from "./item/DeleteItems";
import { EditItems } from "./item/EditItems";

export const Properties = () => {
  const [property, setProperty] = useState([]);
  const [loading, setLoading] = useState(true);
  const [show, setShow] = useState(false);
  const [selectedUserId, setSelectedUserId] = useState();
  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [error, setError] = useState({});
  const { token } = Auth();
  const [query, setQuery] = useState("");
  const [Category, setCategory] = useState("link_categories_id");
  const [cateId, setCateId] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    link_categories_id: "",
    link_info: [
      {
        release_date: "",
        founder: "",
        description_link: "",
        detail: "",
        analysis: "",
      },
    ],
    avatar: "",
  });

  const handleShow = () => setShow(true);
  const handleShowEdit = (userId) => {
    setSelectedUserId(userId);
    setShowEdit(true);
  };
  const handleShowDelete = (userId) => {
    setSelectedUserId(userId);
    setShowDelete(true);
  };
  const handleClose = () => {
    setShow(false);
    setShowEdit(false);
    setShowDelete(false);
  };

  useEffect(() => {
    const fetchProperties = async () => {
      try {
        const data = await axios.get("http://127.0.0.1:8000/api/index/links", {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-type": "application/json",
          },
          params: {
            search: query,
          },
        });
        setProperty(data.data.message.data);
        console.log(data.data.message.data);
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: err.response.data.message,
        });
      } finally {
        setLoading(false);
      }
    };
    const fetchCategory = async () => {
      try {
        const [cate] = await Promise.all([
          axios.get("http://127.0.0.1:8000/api/index/link-category", {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
            params: {
              name: Category,
            },
          }),
        ]);
        setCateId(cate.data.message.data);
      } catch (error) {
        console.log(error.response.data.message);
      }
    };
    fetchCategory();
    fetchProperties();
  }, [query, token, Category]);

  const handleChange = (e) => {
    const { name, value, type, files } = e.target;
    if (type === "file") {
      setFormData({
        ...formData,
        avatar: files[0],
      });
    } else if (name.includes("link_info")) {
      const index = name.split(".")[1];
      const field = name.split(".")[2];
      const updatedLinkInfo = formData.link_info.map((info, i) =>
        i == index ? { ...info, [field]: value } : info
      );
      setFormData({
        ...formData,
        link_info: updatedLinkInfo,
      });
    } else {
      setFormData({
        ...formData,
        [name]: value,
      });
    }
  };

  const handleSubmit = async (e) => {
    console.log(formData);
    e.preventDefault();

    const propertyData = new FormData();

    propertyData.append("name", formData.name);
    propertyData.append("link_categories_id", formData.link_categories_id);

    formData.link_info.forEach((info, index) => {
      propertyData.append(
        `link_info[${index}][release_date]`,
        info.release_date
      );
      propertyData.append(`link_info[${index}][founder]`, info.founder);
      propertyData.append(
        `link_info[${index}][description_link]`,
        info.description_link
      );
      propertyData.append(`link_info[${index}][detail]`, info.detail);
      propertyData.append(`link_info[${index}][analysis]`, info.analysis);
    });
    for (const [key, value] of propertyData.entries()) {
      console.log(`${key}: ${value}`);
    }
    if (formData.avatar) {
      propertyData.append("avatar", formData.avatar);
    }

    try {
      const res = await axios.post(
        "http://127.0.0.1:8000/api/store/link",
        propertyData,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );

      const id = res.data.data.id;
      if (formData.avatar) {
        const imageProperty = new FormData();
        imageProperty.append("image", formData.avatar);

        await axios.post(
          `http://127.0.0.1:8000/api/image/link/${id}`,
          imageProperty,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "multipart/form-data",
            },
          }
        );
      }

      Swal.fire({
        icon: "success",
        title: "Property created!",
        text: "",
        timer: 1100,
      }).then(() => {
        handleClose();
        window.location.reload();
      });
    } catch (error) {
      console.error("Error submitting form", error);
      Swal.fire({
        icon: "error",
        title: "Error creating property",
        text: error.message,
      });
    }
  };
  return (
    <>
      <Box sx={{ display: "flex", marginTop: "3rem", marginLeft: "1rem" }}>
        <SideNav />
        <div class="container-fluid ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
            <div class="row ">
              <div class="col-sm-3 mt-5 mb-4 text-gred d-flex justify-content-between">
                <div className="search w-75 mx-1">
                  <form class="form-inline">
                    <input
                      class="form-control mr-sm-2"
                      items="search"
                      placeholder="Search by name"
                      aria-label="Search"
                      value={query}
                      onChange={(e) => setQuery(e.target.value)}
                    />
                  </form>
                </div>
              </div>
              <div
                class="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred"
                style={{ color: "#124076" }}
              >
                <h2>
                  <b>Table Links</b>
                </h2>
              </div>
              <div class="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                <Button variant="primary" onClick={handleShow}>
                  Create Link
                </Button>
              </div>
            </div>
            <div class="row">
              <div class="table-responsive ">
                <table class="table align-middle  table-striped table-hover table-bordered">
                  <thead>
                    <tr className="text-uppercase">
                      <th>Id</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Created By</th>
                      <th>Created At</th>
                      <th>Status</th>
                      <th>Founder WebSite</th>
                      <th>Image</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {loading ? (
                      <tr>
                        <td colSpan="9">
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <CircularProgress />
                          </Box>
                        </td>
                      </tr>
                    ) : (
                      property.map((items, index) => (
                        <tr key={index}>
                          <td>{items.id}</td>
                          <td className="fw-bold">{items.name}</td>
                          <td>{items.links_category.name}</td>
                          <td>{items.user.username}</td>
                          <td>{items.created_at}</td>
                          <td>
                            <span
                              className={`badge ${
                                items.status
                                  ? items.status === "PUBLIC"
                                    ? "bg-success"
                                    : items.status === "PRIVATE"
                                    ? "bg-danger"
                                    : items.status === "inactive"
                                    ? "bg-secondary"
                                    : ""
                                  : ""
                              }`}
                            >
                              {items.status}
                            </span>
                          </td>
                          {items && items.link_info && items.link_info.length > 0 && (
                              <td>{items.link_info[0].founder}</td>
                          )}
                          <td>
                            {items.avatar ? (
                              <img
                                src={items.avatar}
                                alt="Null"
                                style={{
                                  width: "50px",
                                  height: "50px",
                                  objectFit: "cover",
                                }}
                              />
                            ) : (
                              <img
                                src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                alt="No Image"
                                style={{
                                  width: "50px",
                                  height: "50px",
                                  objectFit: "cover",
                                }}
                              />
                            )}
                          </td>

                          <td>
                            <button
                              className="edit"
                              title="Edit"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "blue",
                                objectFit: "cover",
                              }}
                              data-toggle="tooltip"
                              onClick={() => handleShowEdit(items.id)}
                            >
                              <Edit />
                            </button>
                            <button
                              className="delete"
                              title="Delete"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "red",
                              }}
                              onClick={() => handleShowDelete(items.id)}
                            >
                              <Delete />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            {/* Model Box Finsihs */}
            <div className="model_box ">
              <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: "4rem" }}
              >
                <Modal.Header closeButton>
                  <Modal.Title>Add Links</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <form onSubmit={handleSubmit}>
                    <div className="form-group mx-2 my-2">
                      <input
                        type="text"
                        className={`form-control ${
                          error.name ? "is-invalid" : ""
                        }`}
                        id="name"
                        name="name"
                        value={formData.name}
                        onChange={handleChange}
                        placeholder="Enter Name"
                        required
                      />
                    </div>

                    <div className="form-group mx-2 my-2">
                      <select
                        required
                        className="form-control"
                        name="link_categories_id"
                        value={formData.link_categories_id}
                        onChange={handleChange}
                      >
                        <option value="" disabled>
                          Select Type
                        </option>
                        {cateId.map((buyers) => (
                          <option key={buyers.id} value={buyers.id}>
                            {buyers.name}
                          </option>
                        ))}
                      </select>
                      {error.link_categories_id && (
                        <div className="invalid-feedback">
                          {error.link_categories_id}
                        </div>
                      )}
                    </div>

                    {formData.link_info.map((info, index) => (
                      <div key={index}>
                        <div className="form-group mx-2 my-2">
                          <input
                            type="date"
                            className={`form-control ${
                              error.release_date ? "is-invalid" : ""
                            }`}
                            id={`release_date_${index}`}
                            name={`link_info.${index}.release_date`}
                            value={info.release_date}
                            onChange={handleChange}
                            placeholder="Enter Release Date"
                            required
                          />
                          {error.release_date && (
                            <div className="invalid-feedback">
                              {error.release_date}
                            </div>
                          )}
                        </div>

                        <div className="form-group mx-2 my-2">
                          <input
                            type="text"
                            className={`form-control ${
                              error.founder ? "is-invalid" : ""
                            }`}
                            id={`founder_${index}`}
                            name={`link_info.${index}.founder`}
                            value={info.founder}
                            onChange={handleChange}
                            placeholder="Enter Founder"
                            required
                          />
                          {error.founder && (
                            <div className="invalid-feedback">
                              {error.founder}
                            </div>
                          )}
                        </div>

                        <div className="form-group mx-2 my-2">
                          <input
                            type="text"
                            className={`form-control ${
                              error.description_link ? "is-invalid" : ""
                            }`}
                            id={`description_link_${index}`}
                            name={`link_info.${index}.description_link`}
                            value={info.description_link}
                            onChange={handleChange}
                            placeholder="Enter Description Link"
                            required
                          />
                          {error.description_link && (
                            <div className="invalid-feedback">
                              {error.description_link}
                            </div>
                          )}
                        </div>

                        <div className="form-group mx-2 my-2">
                          <textarea
                            className={`form-control ${
                              error.detail ? "is-invalid" : ""
                            }`}
                            id={`detail_${index}`}
                            name={`link_info.${index}.detail`}
                            value={info.detail}
                            onChange={handleChange}
                            placeholder="Enter Detail"
                            required
                          />
                          {error.detail && (
                            <div className="invalid-feedback">
                              {error.detail}
                            </div>
                          )}
                        </div>

                        <div className="form-group mx-2 my-2">
                          <textarea
                            className={`form-control ${
                              error.analysis ? "is-invalid" : ""
                            }`}
                            id={`analysis_${index}`}
                            name={`link_info.${index}.analysis`}
                            value={info.analysis}
                            onChange={handleChange}
                            placeholder="Enter Analysis"
                            required
                          />
                          {error.analysis && (
                            <div className="invalid-feedback">
                              {error.analysis}
                            </div>
                          )}
                        </div>
                      </div>
                    ))}

                    <div className="form-group mx-2 my-2">
                      <input
                        type="file"
                        className={`form-control ${
                          error.avatar ? "is-invalid" : ""
                        }`}
                        id="avatar"
                        name="avatar"
                        onChange={handleChange}
                        required
                      />
                      {error.avatar && (
                        <div className="invalid-feedback">{error.avatar}</div>
                      )}
                    </div>

                    <button type="submit" className="btn btn-success mt-4">
                      Add
                    </button>
                  </form>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
              {/* Model add */}
            </div>

            {/* Model delete */}
            <div className="model_box ">
              {showDelete && (
                <DeleteItems
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>

            {/* Model edit name */}
            <div className="model_box ">
              {showEdit && (
                <EditItems
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>
            {/* Model edit name */}
          </div>
        </div>
      </Box>
    </>
  );
};
