import axios from 'axios';
import React from 'react'
import { Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

export const DeleteRole = ({userId, token,handleClose}) => {
    const handleSubmitDelete = async (e) => {
        e.preventDefault();
        try {
          await axios.delete(`http://127.0.0.1:8000/api/role/remove/${userId}`, {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
          });
          Swal.fire({
            icon: "success",
            title: "Role Deleted Successfully",
            text: "",
            timer: 1200
          }).then(() => {
            handleClose();
            window.location.reload();
          });
        } catch (err) {
          console.error("Error deleting user:", err);  // Log the error for debugging
          Swal.fire({
            icon: "error",
            title: "Delete Unsuccessful",
            text: `${err.response.message}`,
            timer: 1200
          });
        }
      };
  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header >
        <Modal.Title>Delete User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Deleted Role?
        <form onSubmit={handleSubmitDelete}>
          <Button
            type="submit"
            className="btn btn-danger mt-4"
          >
            Delete
          </Button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
