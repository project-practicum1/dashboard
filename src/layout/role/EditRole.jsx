import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export const EditRole = ({ userId, token, handleClose }) => {
  const [name, setName] = useState("");
  const [error, setError] = useState({});
  const validate = () => {
    const errors = {};
    if (!name) {
      errors.name = "Role name is required";
    }
    setError(errors);
    return Object.keys(errors).length === 0;
  };
  useEffect(() => {
    const fetchRole = async () => {
      try {
        const response = await axios.get(
          `http://127.0.0.1:8000/api/role/index/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
          }
        );
        setName(response.data.message.role.name);
        console.log(response.data.message.role.name);
      } catch (e) {
        console.log(e.response);
      }
    };
    fetchRole();
  }, [userId, token]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validate()) {
      await axios
        .put(
          `http://127.0.0.1:8000/api/role/edit/${userId}`,
          {
            name,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then((res) => {
          Swal.fire({
            icon: "success",
            title: "updated successfully",
            text: res.status,
            timer: 1400,
          })
          setTimeout(()=>{
            window.location.reload();
          },1430)
        })
        .catch((err) => {
          Swal.fire({
            icon: "error",
            title: "updated unsuccessfully",
            text: err.response.data.message,
            timer: 1400,
          });
          handleClose();
        });
    }
  };

  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Edit Role</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <input
              type="text"
              className={`form-control ${error.name ? "is-invalid" : ""}`}
              id="name"
              value={name}
              aria-describedby="emailHelp"
              placeholder="Enter Role Name"
              onChange={(e) => {
                setName(e.target.value);
              }}
              required
            />
            {error.name && <div className="invalid-feedback">{error.name}</div>}
          </div>
          <button type="submit" className="btn btn-success mt-4">
            Save
          </button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
