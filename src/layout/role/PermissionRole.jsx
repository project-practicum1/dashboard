import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export const PermissionRole = ({ userId, token, handleClose }) => {
  const [selectedPermissions, setSelectedPermissions] = useState([]);
  const permissionsList = [
    { id: 1, name: "view_user", label: "View User" },
    { id: 2, name: "create_user", label: "Create User" },
    { id: 3, name: "edit_user", label: "Edit User" },
    { id: 4, name: "delete_user", label: "Delete User" },
    { id: 5, name: "view_role", label: "View Role" },
    { id: 6, name: "create_role", label: "Create Role" },
    { id: 7, name: "edit_role", label: "Edit Role" },
    { id: 8, name: "delete_role", label: "Delete Role" },
    { id: 9, name: "view_items", label: "View Items" },
    { id: 10, name: "create_items", label: "Create Items" },
    { id: 11, name: "edit_items", label: "Edit Items" },
    { id: 12, name: "delete_items", label: "Delete Items" },
  ];

  useEffect(() => {
    const fetchRole = async () => {
      try {
        const response = await axios.get(
          `http://127.0.0.1:8000/api/role/index/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "application/json",
            },
          }
        );
        setSelectedPermissions(
          response.data.message.role.permissions.map((permission) => ({
            permission_id: permission.id,
            status: true,
          }))
        );
      } catch (e) {
        console.log(e.response);
      }
    };
    fetchRole();
  }, [userId, token]);
  const handleCheckboxChange = (e) => {
    const { id, checked } = e.target;
    const permissionId = parseInt(id);

    if (checked) {
      setSelectedPermissions((prevSelectedPermissions) => [
        ...prevSelectedPermissions,
        { permission_id: permissionId, status: true },
      ]);
    } else {
      setSelectedPermissions((prevSelectedPermissions) =>
        prevSelectedPermissions.filter(
          (pid) => pid.permission_id !== permissionId
        )
      );
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("Selected Permissions: ", selectedPermissions);

    try {
      const res = await axios.patch(
        `http://127.0.0.1:8000/api/role/permissions/${userId}`,
        { permissions: selectedPermissions },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      Swal.fire({
        icon: "success",
        title: "Updated successfully",
        text: res.status,
        timer: 1400,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1430);
    } catch (err) {
      Swal.fire({
        icon: "error",
        title: "Error updating permissions",
        text: err.response?.data?.message || "An error occurred",
        timer: 1400,
      });
      handleClose();
    }
  };
  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header>
        <Modal.Title>Permission</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="d-flex mt-2 justify-content-between">
            <div className="w-50">
              {permissionsList
                .slice(0, Math.ceil(permissionsList.length / 2))
                .map((permission) => (
                  <div className="form-check" key={permission.id}>
                    <input
                      className="form-check-input"
                      type="checkbox"
                      id={permission.id.toString()}
                      checked={selectedPermissions.some(
                        (p) => p.permission_id === permission.id
                      )}
                      onChange={handleCheckboxChange}
                    />
                    <label
                      className="form-check-label"
                      htmlFor={permission.id.toString()}
                    >
                      {permission.label}
                    </label>
                  </div>
                ))}
            </div>
            <div className="w-50">
              {permissionsList
                .slice(Math.ceil(permissionsList.length / 2))
                .map((permission) => (
                  <div className="form-check" key={permission.id}>
                    <input
                      className="form-check-input"
                      type="checkbox"
                      id={permission.id.toString()}
                      checked={selectedPermissions.some(
                        (p) => p.permission_id === permission.id
                      )}
                      onChange={handleCheckboxChange}
                    />
                    <label
                      className="form-check-label"
                      htmlFor={permission.id.toString()}
                    >
                      {permission.label}
                    </label>
                  </div>
                ))}
            </div>
          </div>
          <button type="submit" className="btn btn-success mt-4">
            Save
          </button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
