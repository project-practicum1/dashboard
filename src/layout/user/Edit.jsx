import axios from "axios";
import React, { useState, useEffect } from "react";
import { Button, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export const Edits = ({ userId, handleClose, token }) => {
  // console.log(userId);
  const [formDataEdit, setFormDataEdit] = useState({
    email: "",
    username: "",
    gender: "",
    phone: "",
    role: "",
    address: "",
    image: null,
  });
  const [errors, setErrors] = useState({});

  const handleInputChange = (e) => {
    const { name, value, files } = e.target;
    if (name === "image") {
      setFormDataEdit((prevState) => ({
        ...prevState,
        [name]: files[0],
      }));
    } else {
      setFormDataEdit((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    }
  };

  const validation = () => {
    const newErrors = {};
    if (!formDataEdit.email) newErrors.email = "Email is required";
    if (!/\S+@\S+\.\S+/.test(formDataEdit.email))
      newErrors.email = "Email is invalid";
    if (!formDataEdit.username) newErrors.username = "Username is required";
    if (!formDataEdit.gender) newErrors.gender = "Gender is required";
    if (!formDataEdit.address) newErrors.address = "Address is required";
    if (!formDataEdit.phone) newErrors.phone = "Phone number is required";
    if (!formDataEdit.image) newErrors.image = "Image is required";
    if (!formDataEdit.role) newErrors.role = "Role is required";

    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const response = await axios.get(
          `http://127.0.0.1:8000/api/index/user/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
          }
        );
        const data = response.data.message.user;
        const roleName = data.roles.map((role) => role.name).join(",");
        setFormDataEdit({
          email: data.email || "",
          gender: data.gender || "",
          address: data.address || "",
          phone: data.phone || "",
          username: data.username || "",
          role: roleName || "",
        });
      } catch (error) {
        console.log(error.message);
      }
    };
    fetchUser();
  }, [userId, token]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validation()) {
      await axios.put(
        `http://127.0.0.1:8000/api/edit/user/${userId}`,
        {
          username: formDataEdit.username,
          email: formDataEdit.email,
          gender: formDataEdit.gender,
          phone: formDataEdit.phone,
          address: formDataEdit.address,
          role: formDataEdit.role
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      ).then((res)=>{
        Swal.fire({
          icon: "success",
          title: "User updated",
          text: "",
          timer: 1000
        })
        handleClose();
        setTimeout(()=>{
          window.location.reload();
        })
      }).catch((err)=>{
        Swal.fire({
          icon: "error",
          title: "User update failed",
          text: `${err.response.data.message}`,
        });
      })
      if (formDataEdit.image) {
        const avatarData = new FormData();
        avatarData.append("image", formDataEdit.image);
        await axios.post(
          `http://127.0.0.1:8000/api/image/user/edit/${userId}`,
          avatarData,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "multipart/form-data"
            },
          }
        )
      }
       
    }
  };

  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Edit User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="form-group mt-3">
            <input
              type="email"
              className={`form-control ${errors.email ? "is-invalid" : ""}`}
              id="email"
              name="email"
              placeholder="Enter email"
              value={formDataEdit.email}
              onChange={handleInputChange}
            />
            {errors.email && (
              <div className="invalid-feedback">{errors.email}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <input
              type="text"
              className={`form-control ${errors.username ? "is-invalid" : ""}`}
              id="username"
              name="username"
              placeholder="Enter username"
              value={formDataEdit.username}
              onChange={handleInputChange}
            />
            {errors.username && (
              <div className="invalid-feedback">{errors.username}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <select
              className={`form-control ${errors.gender ? "is-invalid" : ""}`}
              id="gender"
              name="gender"
              value={formDataEdit.gender}
              onChange={handleInputChange}
            >
              <option>Select</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
            {errors.gender && (
              <div className="invalid-feedback">{errors.gender}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <input
              type="text"
              className={`form-control ${errors.phone ? "is-invalid" : ""}`}
              id="phone"
              name="phone"
              placeholder="Enter phone"
              value={formDataEdit.phone}
              onChange={handleInputChange}
            />
            {errors.phone && (
              <div className="invalid-feedback">{errors.phone}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <input
              type="text"
              className={`form-control ${errors.address ? "is-invalid" : ""}`}
              id="address"
              name="address"
              placeholder="Enter address"
              value={formDataEdit.address}
              onChange={handleInputChange}
            />
            {errors.address && (
              <div className="invalid-feedback">{errors.address}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <input
              type="text"
              className={`form-control ${errors.role ? "is-invalid" : ""}`}
              id="role"
              name="role"
              placeholder="Enter Role"
              value={formDataEdit.role}
              onChange={handleInputChange}
            />
            {errors.role && (
              <div className="invalid-feedback">{errors.role}</div>
            )}
          </div>
          <div className="form-group mt-3">
            <input
              type="file"
              className={`form-control ${errors.image ? "is-invalid" : ""}`}
              id="avatar"
              name="image"
              onChange={handleInputChange}
            />
            {errors.image && (
              <div className="invalid-feedback">{errors.image}</div>
            )}
          </div>
          <button type="submit" className="btn btn-success mt-4">
            Edit
          </button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
