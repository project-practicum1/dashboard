import React from "react";
import axios from "axios";
import { Modal, Button } from "react-bootstrap";
import Swal from "sweetalert2";

export const Deletes = ({ userId, handleClose, token }) => {
  // console.log(userId)
  const handleSubmitDelete = async (e) => {
    e.preventDefault();
    try {
      await axios.delete(`http://127.0.0.1:8000/api/delete/user/${userId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-type": "application/json",
        },
      });
      Swal.fire({
        icon: "success",
        title: "User Deleted Successfully",
        text: "",
        timer: 1000
      }).then(() => {
        handleClose();
        window.location.reload();
      });
    } catch (err) {
      console.error("Error deleting user:", err);  // Log the error for debugging
      Swal.fire({
        icon: "error",
        title: "Delete Unsuccessful",
        text: err.response.data.message,
      });
    }
  };

  return (
    <Modal
      show={true}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      style={{ marginTop: "4rem" }}
    >
      <Modal.Header closeButton>
        <Modal.Title>Delete User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Deleted user?
        <form onSubmit={handleSubmitDelete}>
          <Button
            type="submit"
            className="btn btn-danger mt-4"
          >
            Delete
          </Button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
