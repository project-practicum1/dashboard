import React, { useEffect, useState } from "react";
import SideNav from "../components/SideNav";
import { Box, CircularProgress } from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Dropdown, DropdownButton, Modal } from "react-bootstrap";
import { Delete, Edit, VisibilitySharp } from "@mui/icons-material";
import axios from "axios";
import Swal from "sweetalert2";
import { Auth } from "../api/Auth";
import { Edits } from "./user/Edit";
import { Deletes } from "./user/Delete";

export const User = () => {
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);
  const [error, setError] = useState({});
  const { token, setUser } = Auth();
  const [fileName, setFileName] = useState();
  const [showUser, setShowUser] = useState([]);
  const [loading, setLoading] = useState(true);
  const [formData, setFormData] = useState({

    email: "",
    username: "",
    gender: "",
    phone: "",
    address: "",
    password: "",
    password_confirmation: "",
    image: "",
  });
  const [selectedUserId, setSelectedUserId] = useState();
  const [showEdit, setEditModalShow] = useState(false);
  const [showDelete, setDeleteModalShow] = useState(false);
  const [search, setSearch] = useState("");
  const [role, setRole] = useState("");

  const handleShowEdit = (userId) => {
    setSelectedUserId(userId);
    setEditModalShow(true);
  };
  const handleShowDelete = (userId) => {
    setSelectedUserId(userId);
    setDeleteModalShow(true);
  };

  const handleClose = () => {
    setEditModalShow(false);
    setDeleteModalShow(false);
    setShow(false);
  };

  // Add
  const validation = () => {
    const newErrors = {};
    if (!formData.username) {
      newErrors.username = "Username is required";
    } else if (formData.username.length < 6) {
      newErrors.username = "Username is must be at least 6 characters";
    }
    if (!formData.email) {
      newErrors.email = "Email is required";
    }
    if (!formData.gender) newErrors.gender = "Gender is required";
    if (!formData.phone) newErrors.phone = "phone is required";
    if (!formData.address) newErrors.address = "address is required";

    if (!formData.password) newErrors.password = "Password is required";
    if (formData.password !== formData.password_confirmation)
      newErrors.password_confirmation = "Passwords do not match";
    setError(newErrors);
    return Object.keys(newErrors).length === 0;
  };
  const handleChange = (e) => {
    const { name, value, files } = e.target;
    if (files) {
      const file = files[0];
      setFormData({
        ...formData,
        [name]: file,
      });
      setError({
        ...error,
        [name]: "",
      });
      setFileName(file);
    } else {
      setFormData({
        ...formData,
        [name]: value,
      });
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (validation()) {
      try {
        const userData = new FormData();
        for (const key in formData) {
          if (key !== "image") {
            userData.append(key, formData[key]);
          }
        }

        const response = await axios.post(
          "http://127.0.0.1:8000/api/store/user",
          userData,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-type": "multipart/form-data",
            },
          }
        );
        const responseData = response.data;
        const id = responseData.data.id;

        if (responseData.data.token) {
          setUser(responseData.data.token);
        }

        if (formData.image) {
          const avatarData = new FormData();
          avatarData.append("image", formData.image);
          for (const pair of avatarData.entries()) {
            console.log(pair[0] + ": " + pair[1]);
          }
          await axios
            .post(
              `http://127.0.0.1:8000/api/image/user/${id}`,
              avatarData,
              {
                headers: {
                  Authorization: `Bearer ${token}`,
                  "Content-type": "multipart/form-data",
                },
              }
            )
            .then((res) => {
              console.log(avatarData);
            });
        }

        Swal.fire({
          icon: "success",
          title: "User created!",
          text: "",
        }).then(() => {
          handleClose();
          window.location.reload();
        });
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "User create failed",
          text: `${err.response.data.message}`,
        });
        // console.log(err.response.data)
      }
    }
  };
  // add

  // view
  useEffect(() => {
    const showUsers = async () => {
      setLoading(true);
      try {
        const response = await axios.get(
          "http://127.0.0.1:8000/api/index/users",
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
            },
            params: {
              search,
            },
          }
        );
        setShowUser(response.data.message.data);
        
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "Null",
          text: "",
        });
      } finally {
        setLoading(false);
      }
    };
    showUsers();
  }, [search, token]);

  const handleSearch = (e) => {
    setSearch(e.target.value);
  };
  // view
  return (
    <>
      <Box sx={{ display: "flex", marginTop: "3rem", marginLeft: "1rem" }}>
        <SideNav />
        <div class="container-fluid ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
            <div class="row ">
              <div class="col-sm-3 mt-5 mb-4 text-gred d-flex justify-content-between">
                <div className="search w-75 mx-1">
                  <form class="form-inline">
                    <input
                      class="form-control mr-sm-2"
                      type="search"
                      placeholder="Search by username"
                      aria-label="Search"
                      value={search}
                      onChange={handleSearch}
                    />
                  </form>
                </div>
              </div>

              <div
                class="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred"
                style={{ color: "#124076" }}
              >
                <h2>
                  <b>Table User</b>
                </h2>
              </div>
              <div class="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                <Button variant="primary" onClick={handleShow}>
                  Create user
                </Button>
              </div>
            </div>
            <div class="row">
              <div class="table-responsive ">
                <table class="table table-striped table-hover table-bordered text-center align-middle">
                  <thead>
                    <tr className="text-uppercase">
                      <th>Id</th>
                      <th>Role</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Image</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {loading ? (
                      <tr>
                        <td colSpan="9">
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <CircularProgress />
                          </Box>
                        </td>
                      </tr>
                    ) : (
                      showUser.map((user, index) => (
                        <tr key={index}>
                          <td>{user.id}</td>
                          <td>
                            <span
                              className={`badge ${
                                user.roles && user.roles.length > 0
                                  ? user.roles[0].name === "admin" ||
                                    user.roles[0].name === "super-admin"
                                    ? "bg-success"
                                    : user.roles[0].name === "buyer"
                                    ? "bg-secondary"
                                    : user.roles[0].name === "seller"
                                    ? "bg-dark"
                                    : "bg-danger"
                                  : ""
                              }`}
                            >
                              {user.roles && user.roles.length > 0
                                ? user.roles[0].name
                                : ""}
                            </span>
                          </td>
                          <td>{user.username}</td>
                          <td>{user.email}</td>
                          <td>{user.phone ?user.phone: "Null" }</td>
                          <td>
                            <img
                              src={
                                user.avatar == null
                                  ? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                  : user.avatar
                              }
                              style={{
                                width: "80px",
                                height: "80px",
                                objectFit: "cover",
                                borderRadius: "0.6rem",
                              }}
                              alt=""
                            />
                          </td>
                          <td>
                            <button
                              className="edit"
                              title="Edit"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "blue",
                                objectFit: "cover",
                              }}
                              data-toggle="tooltip"
                              onClick={() => handleShowEdit(user.id)}
                            >
                              <Edit />
                            </button>
                            <button
                              className="delete"
                              title="Delete"
                              data-toggle="tooltip"
                              style={{
                                border: "none",
                                background: "transparent",
                                color: "red",
                              }}
                              onClick={() => handleShowDelete(user.id)}
                            >
                              <Delete />
                            </button>
                          </td>
                        </tr>
                      ))
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            {/* create modal */}
            <div className="model_box">
              <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                style={{ marginTop: "4rem" }}
              >
                <Modal.Header closeButton>
                  <Modal.Title>Add User</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <form onSubmit={handleSubmit} encType="multipart/form-data">
                    <div className="form-group mt-3">
                      <input
                        type="email"
                        className={`form-control ${
                          error.email ? "is-invalid" : ""
                        }`}
                        id="email"
                        name="email"
                        placeholder="Enter email"
                        value={formData.email}
                        onChange={handleChange}
                      />
                      {error.email && (
                        <div className="invalid-feedback">{error.email}</div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="text"
                        className={`form-control ${
                          error.username ? "is-invalid" : ""
                        }`}
                        id="username"
                        name="username"
                        placeholder="Enter username"
                        value={formData.username}
                        onChange={handleChange}
                      />
                      {error.username && (
                        <div className="invalid-feedback">{error.username}</div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <select
                        className={`form-control ${
                          error.gender ? "is-invalid" : ""
                        }`}
                        id="gender"
                        name="gender"
                        value={formData.gender}
                        onChange={handleChange}
                      >
                        <option>Select</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                      </select>
                      {error.gender && (
                        <div className="invalid-feedback">{error.gender}</div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="text"
                        className={`form-control ${
                          error.phone ? "is-invalid" : ""
                        }`}
                        id="phone"
                        name="phone"
                        placeholder="Enter phone"
                        value={formData.phone}
                        onChange={handleChange}
                      />
                      {error.phone && (
                        <div className="invalid-feedback">{error.phone}</div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="text"
                        className={`form-control ${
                          error.phone ? "is-invalid" : ""
                        }`}
                        id="address"
                        name="address"
                        placeholder="Enter address"
                        value={formData.address}
                        onChange={handleChange}
                      />
                      {error.address && (
                        <div className="invalid-feedback">{error.address}</div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="password"
                        className={`form-control ${
                          error.password ? "is-invalid" : ""
                        }`}
                        id="password"
                        name="password"
                        placeholder="Enter password"
                        value={formData.password}
                        onChange={handleChange}
                      />
                      {error.password && (
                        <div className="invalid-feedback">{error.password}</div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="password"
                        className={`form-control ${
                          error.password_confirmation ? "is-invalid" : ""
                        }`}
                        id="password_confirmation"
                        name="password_confirmation"
                        placeholder="Confirm password"
                        value={formData.password_confirmation}
                        onChange={handleChange}
                      />
                      {error.password_confirmation && (
                        <div className="invalid-feedback">
                          {error.password_confirmation}
                        </div>
                      )}
                    </div>
                    <div className="form-group mt-3">
                      <input
                        type="file"
                        className={`form-control ${
                          error.image ? "is-invalid" : ""
                        }`}
                        id="avatar"
                        name="image"
                        onChange={handleChange}
                      />
                      {error.image && (
                        <div className="invalid-feedback">{error.image}</div>
                      )}
                    </div>
                    <button
                      type="button"
                      onClick={handleSubmit}
                      className="btn btn-success mt-4"
                    >
                      Add
                    </button>
                  </form>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                </Modal.Footer>
              </Modal>
            </div>

            {/* Modal for Editing */}
            <div className="model_box">
              {showEdit && (
                <Edits
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>

            {/* Modal for Deleting */}
            <div className="model_box">
              {showDelete && (
                <Deletes
                  userId={selectedUserId}
                  token={token}
                  handleClose={handleClose}
                />
              )}
            </div>
          </div>
        </div>
      </Box>
    </>
  );
};
