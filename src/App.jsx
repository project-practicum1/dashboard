
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Home } from './layout/Home';
import {User} from './layout/User';
import { Error } from './layout/Error';
import {Role} from './layout/Role';
import {Offer} from './layout/product/Offer';
import {Transaction} from './layout/product/Transaction';
import { Properties }  from './layout/product/Properties'
import { PropertyTypes } from './layout/product/PropertyTypes';
import { ListsProducts } from './layout/ListsProducts';
import { Profile } from './layout/Profile';
import { Register } from './components/auth/register/Register';
import { Login } from './components/auth/register/Login';
import { ForgetPassword } from './components/auth/forgotpassword/ForgetPassword';
import { ResetPassword } from './components/auth/forgotpassword/ResetPassword';
// import { AuthProvider } from './api/AuthProvider';

function App() {
  return (  
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element = {<Home/>}/>
          <Route path='/user' element = {<User/>}/>
          <Route path='/error' element= {<Error/>}/> 
          <Route path='/role' element= {<Role/>}/> 
          <Route path='/products-type' element={<PropertyTypes/>}/> 
          <Route path='/products' element= {<Properties/>}/> 
          <Route path='/offer' element= {<Offer/>}/> 
          <Route path='/transaction' element= {<Transaction/>}/>
          <Route path='/list-products' element= {<ListsProducts/>}/> 
          <Route path='/profile' element= {<Profile/>}/> 
          <Route path='/register-form' element= {<Register/>}/>
          <Route path='/login' element = {<Login/>}/> 
          <Route path='/forget-password' element={<ForgetPassword/>}/>
          <Route path='/forget-password/reset' element={<ResetPassword/>}/>
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App
