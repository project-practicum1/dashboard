import axios from 'axios';
import  { useState } from 'react'
import { useNavigate } from 'react-router-dom';

export const Auth = () => {
    
    const getToken = () => {
      const tokenString = sessionStorage.getItem("token");
      const userToken = tokenString? JSON.parse(tokenString):null;
      return userToken;
    };
    const [token, setToken] = useState(getToken());
    const navigate = useNavigate();
    const saveToken = (token) => {
      sessionStorage.setItem("token", JSON.stringify(token)); 
      setToken(token);
      navigate("/");
      
    };
    const saveUserToken = (token) => {
      sessionStorage.setItem("token", JSON.stringify(token)); 
      setToken(token);      
      navigate("/user")
    };
    const logOut = ()=>{
      sessionStorage.clear();
      setToken(null);
      navigate("/login");
    }
    const http = axios.create({
        baseURL: "http://127.0.0.1:8000/api",
        headers: {
            "Content-type": "application/json",
            "Authorization" : `Bearer ${token}`,
        }
    });
  return {
    setToken: saveToken,
    setUser: saveUserToken,
    token,
    getToken,
    http: http,
    logOut,
  }
}
